package com.azhapps.roomdemo.live;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

public abstract class DataListAdapter<Data, Binding extends ViewDataBinding> extends BaseAdapter {

    protected final String TAG = getClass().getSimpleName();

    private LayoutInflater layoutInflater;
    protected List<Data> list;

    public DataListAdapter(Context context, List<Data> list) {
        layoutInflater = LayoutInflater.from(context);
        this.list = list;
    }
    public void setList(List<Data> list) {
        if (this.list != list) {
            this.list = list;
            notifyDataSetChanged();
        }
    }

    @LayoutRes
    protected abstract int layoutId();

    protected abstract void bindData(Binding binding, Data data, int position);

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Data getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Binding binding =  convertView != null
                ?(Binding) DataBindingUtil.findBinding(convertView)
                :(Binding) DataBindingUtil.inflate(layoutInflater, layoutId(), parent, false);

        bindData(binding, getItem(position), position);


        return binding.getRoot();
    }
}
