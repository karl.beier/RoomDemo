package com.azhapps.roomdemo.live

import android.app.AlertDialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.view.LayoutInflater
import com.azhapps.roomdemo.R
import com.azhapps.roomdemo.databinding.ActivityDemoBinding
import com.azhapps.roomdemo.databinding.ViewModifyBinding
import com.azhapps.roomdemo.example.java.DatabaseJava
import com.azhapps.roomdemo.example.kotlin.DatabaseKt
import com.azhapps.roomdemo.example.kotlin.PersonKt

class DemoActivity : FragmentActivity() {

    private lateinit var binding: ActivityDemoBinding
    private lateinit var viewModel: DemoViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        DatabaseJava.init(applicationContext)
        DatabaseKt.init(applicationContext)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_demo)
        viewModel = ViewModelProviders.of(this).get(DemoViewModel::class.java)

        binding.btnAdd.setOnClickListener({
            val modifyBinding: ViewModifyBinding = DataBindingUtil.inflate(LayoutInflater.from(this@DemoActivity), R.layout.view_modify, null, false)

            AlertDialog.Builder(this@DemoActivity)
                    .setTitle("Add Person")
                    .setView(modifyBinding.root)
                    .setPositiveButton("Ok", { _, _ ->
                        val personKt = PersonKt(
                                modifyBinding.edtName.text.toString(),
                                modifyBinding.edtEmail.text.toString(),
                                modifyBinding.edtAddress.text.toString()
                        )

                        viewModel.insert(personKt)
                    })
                    .show()
        })

        //ViewModels handle the whole lifecycle for us - we just need to Observe
        val live = true
        if (live) {
            viewModel.livePeople.observe(this, Observer {
                it?.let {
                    setupAdapter(it)
                }
            })
        } else {
            viewModel.lazyPeople.observe(this, Observer {
                it?.let {
                    setupAdapter(it)
                }
            })
            viewModel.updateLazyPeople()
        }
    }

    private fun setupAdapter(list: List<PersonKt>) {
        val adapter = binding.listView.adapter

        if (adapter is PersonListAdapter) {
            adapter.setList(list)
        } else {
            binding.listView.adapter = PersonListAdapter(this, list, object : PersonListAdapter.PersonListener {
                override fun onEdit(oldPerson: PersonKt) {
                    val modifyBinding: ViewModifyBinding =
                            DataBindingUtil.inflate(LayoutInflater.from(this@DemoActivity), R.layout.view_modify, null, false)

                    modifyBinding.edtName.setText(oldPerson.name)
                    modifyBinding.edtAddress.setText(oldPerson.address)
                    modifyBinding.edtEmail.setText(oldPerson.email)

                    AlertDialog.Builder(this@DemoActivity)
                            .setTitle("Add Person")
                            .setView(modifyBinding.root)
                            .setPositiveButton("Ok", { _, _ ->
                                val newPerson = PersonKt(
                                        modifyBinding.edtName.text.toString(),
                                        modifyBinding.edtEmail.text.toString(),
                                        modifyBinding.edtAddress.text.toString()
                                )

                                viewModel.update(oldPerson, newPerson)
                            })
                            .show()
                }

                override fun onDelete(person: PersonKt) {
                    viewModel.delete(person)
                }
            })
        }
    }
}