package com.azhapps.roomdemo.live

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.azhapps.roomdemo.example.kotlin.DatabaseKt
import com.azhapps.roomdemo.example.kotlin.PersonKt
import java.util.concurrent.Executors

class DemoViewModel : ViewModel() {

    val livePeople = DatabaseKt.instance.personDao().getAllLive() //All we need to do to get the data! Threads etc. 100% managed for us
    val lazyPeople = MutableLiveData<List<PersonKt>>()

    private val threadPool = Executors.newCachedThreadPool()

    //For non-live DB calls we need to manage threading on our own
    fun delete(personKt: PersonKt) {
        threadPool.execute({
            DatabaseKt.instance.personDao().delete(personKt)
        })
    }

    fun insert(personKt: PersonKt) {
        threadPool.execute({
            DatabaseKt.instance.personDao().insert(personKt)
        })
    }

    //The update helper method works on primary key, so if it changes, we need to insert/delete
    fun update(oldPerson: PersonKt, newPerson: PersonKt) {
        threadPool.execute({
            if (oldPerson.name == newPerson.name) {
                DatabaseKt.instance.personDao().update(newPerson)
            } else {
                DatabaseKt.instance.personDao().delete(oldPerson)
                DatabaseKt.instance.personDao().insert(newPerson)
            }
        })
    }

    fun updateLazyPeople() {
        threadPool.execute({
            lazyPeople.postValue(DatabaseKt.instance.personDao().getAll())
        })
    }
}