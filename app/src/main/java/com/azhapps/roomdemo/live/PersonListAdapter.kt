package com.azhapps.roomdemo.live

import android.content.Context
import com.azhapps.roomdemo.R
import com.azhapps.roomdemo.databinding.ItemPersonBinding
import com.azhapps.roomdemo.example.kotlin.PersonKt

class PersonListAdapter(context: Context, people: List<PersonKt>, val listener: PersonListener)
    : DataListAdapter<PersonKt, ItemPersonBinding>(context, people) {

    override fun layoutId(): Int = R.layout.item_person

    override fun bindData(binding: ItemPersonBinding?, data: PersonKt?, position: Int) {
        binding?.person = data
        binding?.btnEdit?.setOnClickListener({
            data?.let {
                listener.onEdit(it)
            }
        })
        binding?.btnDelete?.setOnClickListener({
            data?.let {
                listener.onDelete(it)
            }
        })
    }


    interface PersonListener {

        fun onEdit(oldPerson: PersonKt)

        fun onDelete(person: PersonKt)
    }
}