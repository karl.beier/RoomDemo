package com.azhapps.roomdemo.example.java;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import com.azhapps.roomdemo.example.BaseDao;

@Dao
public interface PersonDaoJava extends BaseDao<PersonJava> {

    @Query("SELECT * FROM personjava WHERE name LIKE :name LIMIT 1")
    PersonJava getByName(String name);
}
