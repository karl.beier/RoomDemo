package com.azhapps.roomdemo.example.kotlin

import android.arch.persistence.room.TypeConverter
import java.util.*

/*
    Type converters allow us to store objects of any type, we just need to
    specify a conversion from object to string and back.

    Below is one way to provide converters for Date
 */
class Converters {

    @TypeConverter
    fun fromJson(json: String): Date = Date(json.toLong())

    @TypeConverter
    fun toJson(date: Date): String = date.time.toString()
}