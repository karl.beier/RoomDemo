package com.azhapps.roomdemo.example.java;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {PersonJava.class}, version = 1)
abstract public class DatabaseJava extends RoomDatabase {

    private static DatabaseJava instance;

    public static void init(Context context) {
        instance = Room.databaseBuilder(context, DatabaseJava.class, "database_java").build();
    }

    public static DatabaseJava getInstance() {
        return instance;
    }

    abstract public PersonDaoJava personDao();
}
