package com.azhapps.roomdemo.example

import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Update

/*
    Inheritance works as expected in Dao interfaces. This means we can eliminate some repeated code
    by using a base class with the helper methods
 */
interface BaseDao<in T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg t: T)

    @Delete
    fun delete(vararg t: T)

    @Update
    fun update(vararg t: T)

}