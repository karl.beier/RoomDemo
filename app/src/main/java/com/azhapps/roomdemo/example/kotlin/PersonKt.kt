package com.azhapps.roomdemo.example.kotlin

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/*
    Data classes helpfully generate getters, setters, equals(), toString() and copy() for us
 */
@Entity(tableName = "person_kt")
data class PersonKt(@PrimaryKey val name: String,
                    val email: String,
                    val address: String
)