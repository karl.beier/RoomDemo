package com.azhapps.roomdemo.example.kotlin

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context

@Database(entities = [PersonKt::class], version = 1)
@TypeConverters(Converters::class)
abstract class DatabaseKt : RoomDatabase() {

    companion object {
        lateinit var instance: DatabaseKt

        fun init(context: Context) {
            instance = Room.databaseBuilder(context, DatabaseKt::class.java, "kt_database").build()
        }
    }

    abstract fun personDao(): PersonDaoKt
}