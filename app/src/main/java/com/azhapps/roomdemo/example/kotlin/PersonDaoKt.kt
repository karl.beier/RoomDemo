package com.azhapps.roomdemo.example.kotlin

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.azhapps.roomdemo.example.BaseDao

@Dao
interface PersonDaoKt : BaseDao<PersonKt> {

    @Query("SELECT * FROM person_kt WHERE email LIKE :email LIMIT 1")
    fun getByEmail(email: String): PersonKt?

    @Query("SELECT * FROM person_kt WHERE email IN (:emails)")
    fun getByEmail(emails: List<String>): List<PersonKt>

    @Query("SELECT * FROM person_kt WHERE name LIKE :name AND email LIKE :email AND address LIKE :address LIMIT 1")
    fun getExact(name: String, email: String, address: String): PersonKt?

    //We can get the whole table as list - but this only reflects the DB at time of fetch
    @Query("SELECT * FROM person_kt")
    fun getAll(): List<PersonKt>

    //However, we can get a LiveData of the table - this will be continually updated as the DB is updated
    @Query("SELECT * FROM person_kt")
    fun getAllLive(): LiveData<List<PersonKt>>
}